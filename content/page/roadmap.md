+++
date = "2015-07-18T14:08:35+02:00"
draft = false
title = "Roadmap"
+++
Ideas in this web page are ideal for those who want to take some new feature to implement which can be a middle-term task:
## 3.24
<table class="table table-bordered">
  <tr>
    <th>Task</th>
    <th>Description</th>
    <th>Bug</th>
    <th>Assigned</th>
    <th>Status</th>
  </tr>
  <tr>
    <td>Use GtkPathBar instead of NautilusPathbar</td>
    <td>Use the stock GtkPathBar and improve it with Nautilus needs so other projects can benefit as well.
    Also rework the UI as per [mockups](https://wiki.gnome.org/Design/OS/Breadcrumbs)</td>
    <td></td>
    <td>CarlosSoriano</td>
    <td class="warning">In progress</td>
  </tr>
  <tr>
    <td>Action bars</td>
    <td>Implement action bars for nautilus instead of some menus
    <a href="https://github.com/gnome-design-team/gnome-mockups/blob/master/nautilus/nautilus-next/documents.png"> mockup 1</a>
    <a href="https://github.com/gnome-design-team/gnome-mockups/blob/master/nautilus/nautilus-next/foldern-menu.png"> mockup 2</a>
    <td><a href="http://bugzilla.gnome.org/show_bug.cgi?id=767874"> 767874</a></td>
    <td>GeorgesStravacas</td>
    <td class="danger">Blocked for decision</td>
  </tr>
  <tr>
    <td>Remove floating bar</td>
    <td>Remove the floating bar, providing other ways for its uses i.e. a spinner while loading a directory. This depends on the action bar.</td>
    <td><a href="http://bugzilla.gnome.org/show_bug.cgi?id=750848">750848</a></td>
    <td></td>
    <td class="danger">Blocked for decision</td>
  </tr>
</table>

## 3.26
<table class="table table-bordered">
  <tr>
    <th>Task</th>
    <th>Description</th>
    <th>Bug</th>
    <th>Assigned</th>
    <th>Status</th>
  </tr>
  <tr>
    <td>Use GtkPathBar instead of NautilusPathbar</td>
    <td>Use the stock GtkPathBar and improve it with Nautilus needs so other projects can benefit as well.
    Also rework the UI as per [mockups](https://wiki.gnome.org/Design/OS/Breadcrumbs)</td>
    <td></td>
    <td>CarlosSoriano</td>
    <td class="warning">In progress</td>
  </tr>
  <tr>
    <td>Action bars</td>
    <td>Implement action bars for nautilus instead of some menus
    <a href="https://github.com/gnome-design-team/gnome-mockups/blob/master/nautilus/nautilus-next/documents.png"> mockup 1</a>
    <a href="https://github.com/gnome-design-team/gnome-mockups/blob/master/nautilus/nautilus-next/foldern-menu.png"> mockup 2</a>
    <td><a href="http://bugzilla.gnome.org/show_bug.cgi?id=767874"> 767874</a></td>
    <td>GeorgesStravacas</td>
    <td class="danger">Blocked for decision</td>
  </tr>
  <tr>
    <td>Remove floating bar</td>
    <td>Remove the floating bar, providing other ways for its uses i.e. a spinner while loading a directory. This depends on the action bar.</td>
    <td><a href="http://bugzilla.gnome.org/show_bug.cgi?id=750848">750848</a></td>
    <td></td>
    <td class="danger">Blocked for decision</td>
  </tr>
</table>
