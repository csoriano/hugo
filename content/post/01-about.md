+++
date = "2015-07-18T14:08:29+02:00"
draft = false
img = "landscape.png"
weight = 1
orientation = "center"
+++
Nautilus (Files) is a file manager created to fit the GNOME desktop design and behavior, giving the user a simple way to navigate and manage files.
